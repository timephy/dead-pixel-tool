# dead-pixel-tool (v1)

This tool is for testing 'should-be-black' images of cameras for 'dead-pixels' - pixels that are not 'black'.

A pixel is deemed 'black' when neighter of its RGB values exceeds the fixed threshold of 10 (alpha is ignored).

### This tool is two-staged:

- Analysis (NodeJS, for better performance)
- Visualisation (Web-Browser, for universality)

## How to use:
### Setup
    
Install [NodeJS](https://nodejs.org/en/download/).

Install [Chrome](https://www.google.com/chrome/) or [Edge](https://www.microsoft.com/en-us/windows/microsoft-edge).

Put the images you want to analyse inside the 'images'-folder.

Image file names have to be in this format: 'CamId-[B/D]ImageId.jpg' (Note the dash!).
In front of the ImageId you can optionally put a B for brigth or a D for dark - the analyzer will ignore D-tagged images.
        
### Windows
    Double-click 'run-windows.cmd' (or run from command-line).
    
    (When opening 'index.html' do not use 'Internet Explorer' - for reasons.)
    
### macOS / UNIX
    Double-click 'run-mac_unix.sh' (or run from command-line).

## Additional Information:

- Uses child-processes to analyse the images faster (as many as there are CPU cores).
- Uses the 'jpeg-js' library to get pixel data.
- This tool gives slightly other values as results, than the tool used before, because is decodes the jpeg in a slightly different way.

This tool was created for nLIGHT, Inc.
