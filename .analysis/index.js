const fs = require("fs");
const fork = require("child_process").fork;
const NUM_THREADS = require("os").cpus().length; // - 1;

const processes = [];
const data = [];

const config = JSON.parse(
    fs.readFileSync("config.json").toString()
);

const dataDir = "./data/"
const imagesDir = "./images/";
var imageFileNames;
try {
    imageFileNames = fs.readdirSync(imagesDir);
    imageFileNames = imageFileNames.filter(name => name.split("-")[1][0] !== "B");
} catch (e) {
    console.log("Folder 'images' did not exist, so it was created for you. Put in images and re-run.")
    fs.mkdirSync("images")
    process.exit(1);
}
const countImages = imageFileNames.length;

if (!countImages) {
    console.log("No images found in folder 'images'.");
    process.exit(1);
}

const end = () => {
    for (const child of processes) {
        child.kill();
    }
    const outData = data.sort(function (a, b) {
        if (a.id < b.id) return -1;
        if (a.id > b.id) return 1;
        return 0;
    });
    if (!fs.existsSync(dataDir)) fs.mkdirSync(dataDir);
    // write raw data to js file for inspection
    fs.writeFileSync(".visual/" + "dead-pixels-data.js", "const deadPixelData = " + JSON.stringify(outData, null, 2));
    // filter data for wanted output for json and csv
    for (img of outData) {
        img.deadPixels = img.deadPixels.filter(pxl => pxl.value >= config.threshold);
    }
    const jsonOut = {
        config: config,
        images: outData
    }
    fs.writeFileSync(dataDir + "dead-pixels.json", JSON.stringify(jsonOut, null, 2));
    // filter and format data for csv
    csvData = ["IMAGE_ID,GROUPS,DEAD_PIXELS(x:y:value:failure)..."];
    csvData.push([
        `THRESHOLD=${config.threshold}:` +
        `X_INSET=${config.xInset}` +
        `Y_INSET=${config.yInset}`
    ]);
    for (img of outData) {
        const line = [];
        line.push(img.id);
        for (pxl of img.deadPixels) {
            const failure = (function () {
                const { x, y } = pxl;
                for (comp of img.deadPixels) {
                    if (
                        comp.x === x && comp.y === y + 1 ||
                        comp.x === x && comp.y === y - 1 ||
                        comp.x === x + 1 && comp.y === y ||
                        comp.x === x - 1 && comp.y === y ||
                        comp.x === x + 1 && comp.y === y + 1 ||
                        comp.x === x - 1 && comp.y === y + 1 ||
                        comp.x === x + 1 && comp.y === y - 1 ||
                        comp.x === x - 1 && comp.y === y - 1
                    ) return true;
                }
                return false;
            })() ?
                7 :
                pxl.value > 512 / 4 ?
                    1 :
                    pxl.value > 155 / 4 ?
                        2 :
                        0;
            line.push(`${pxl.x}:${pxl.y}:${pxl.value}:${failure}`);
        }
        csvData.push(line.join(","));
    }
    fs.writeFileSync(dataDir + "dead-pixels.csv", csvData.join("\n"));
    console.log("##### DONE #####")
};

const assignProcess = (proc) => {
    // console.log(`Assigning process ${proc.pid}.`);
    const newImageName = imageFileNames.pop();
    if (newImageName) {
        proc.send(imagesDir + newImageName, function () { });
    } else if (data.length === countImages) {
        end();
    }
}

const processAnalysisComplete = (proc, result) => {
    data.push(result);
    assignProcess(proc);
}

console.log(`Creating ${NUM_THREADS} thread(s)...`);
for (let i = 0; i < NUM_THREADS; i++) {
    const child = processes[i] = fork("./.analysis/analyze-jpeg.js");
    child.on("message", (result) => processAnalysisComplete(child, result));
}
console.log("Created Processes.");

for (proc of processes) {
    assignProcess(proc);
}
