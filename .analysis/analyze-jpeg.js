//const process = require("process");
const JPEG = require("./jpeg");
const fs = require("fs");

const VALUE_THRESHOLD = 10;

function loadImage(path) {
    const file = fs.readFileSync(path);
    const image = JPEG(file, true);
    return image;
}

function analyzeImage(image) {
    const deadPixels = [];
    let i = 0;
    while (i < image.data.length) {
        const data = image.data;
        const r = data[i + 0];
        const g = data[i + 1];
        const b = data[i + 2];

        let val = r;
        val = g > val ? g : val;
        val = b > val ? b : val;

        if (val > VALUE_THRESHOLD) {
            const obj = {
                x: Math.floor(i / 4) % image.width + 1,
                y: Math.floor(i / 4 / image.width) + 1,
                value: val
            };
            deadPixels.push(obj);
        }
        //
        i += 4;
    }
    return deadPixels;
}

process.on("message", function (imageFilePath) {
    console.log(`Process ${process.pid} now analyzing: ${imageFilePath}`);
    const image = loadImage(imageFilePath);
    const data = analyzeImage(image);

    const imageFilePathParts = imageFilePath.split("/");
    const imageFileName = imageFilePathParts[imageFilePathParts.length - 1].split(".")[0];
    process.send({
        id: imageFileName,
        width: image.width,
        height: image.height,
        deadPixels: data
    });
});
