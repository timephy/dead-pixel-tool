const cameras = []; // Camera[]

function Camera(id) {
    this.id = id; // string
    this.images = []; // [key: string]: Image

    this.htmlContainer = appendCam(id);
    this.addImage = function (imageObject) {
        const imageNum = imageObject.id;
        const image = new Image(imageNum, imageObject, this.htmlContainer);
        this.images.push(image);
        return image;
    }
    cameras.push(this);
};

function Image(imageNum, imageObject, cameraContainer) {
    this.id = imageNum;
    this.width = imageObject.width;
    this.height = imageObject.height;

    this.deadPixels = imageObject.deadPixels; // All pixels
    this.areaPixels = areaSelect(imageObject); // Map with area-select, critical and other
    this.areaThresholdPixels = { // Map with area-select and threshold, critical and other
        critical: [],
        other: []
    };

    this.htmlBars = appendImage(imageNum, cameraContainer);

    this.setObsolete = function (bool) {
        this.htmlBars.critical.style.backgroundColor = bool ? "gray" : "tomato";
        this.htmlBars.other.style.backgroundColor = bool ? "lightgray" : "limegreen";
    };

    this.setNumDeadPixels = function (criticalNum, otherNum) {
        const barPercentages = getBarPercentages(criticalNum, otherNum);
        this.htmlBars.critical.style.width = `${barPercentages.critical}%`;
        this.htmlBars.critical.title = `${criticalNum} critical dead pixels`;
        this.htmlBars.other.style.width = `${barPercentages.other}%`;
        this.htmlBars.other.title = `${otherNum} non-critical dead pixels`;
    };

    this.refresh = function () {
        this.setNumDeadPixels(this.areaThresholdPixels.critical.length, this.areaThresholdPixels.other.length);
        this.setObsolete(false);
    }
}

function consumeImageJSON(contents) {
    const cams = {};
    for (imageData of contents) {
        const imageKeySplit = imageData.id.split("-");
        const camId = imageKeySplit[0];
        const imageId = imageKeySplit[1];

        // Overwriting from 'CXXX-Y' to 'Y' for listing
        imageData.id = imageId;

        // Add camera element if it does not exist
        if (!cams[camId]) cams[camId] = new Camera(camId);
        const image = cams[camId].addImage(imageData);
    }
    refreshAll();
}

/*interface Pixel {
    x: number,
    y: number,
    value: number
}*/
