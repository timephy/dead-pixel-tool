function appendCam(camId) {
    const elem = document.createElement("div");
    elem.className = "image-item";

    const span = document.createElement("span");
    span.className = "image-name";
    span.innerHTML = camId;

    const ul = document.createElement("ul");
    ul.className = "image-chart";

    elem.appendChild(span);
    elem.appendChild(ul);
    document.getElementById("image-objects").appendChild(elem);

    return elem;
}

function appendImage(imageNum, htmlContainer) {
    const elem = document.createElement("li");
    elem.className = "chart-element";

    const divNum = document.createElement("div");
    divNum.className = "image-number";
    divNum.innerHTML = imageNum;

    const divCritical = document.createElement("div");
    divCritical.className = "chart-bar";
    divCritical.style.width = "10%";
    divCritical.style.backgroundColor = "gray";

    const divOther = document.createElement("div");
    divOther.className = "chart-bar-other";
    divOther.style.width = "15%";
    divOther.style.backgroundColor = "lightgray";

    elem.appendChild(divNum);
    elem.appendChild(divCritical);
    elem.appendChild(divOther);
    htmlContainer.appendChild(elem);

    return {
        critical: divCritical,
        other: divOther
    };
}

/*function setObsolete(camId, imgNum, bool) {
    cameras.find((cam) => cam.id === camId).images.find((img) => img.id === imgNum).setObsolete(bool);
}*/

function forEachImage(func) {
    for (cam of cameras) {
        for (img of cam.images) {
            func(img);
        }
    }
}

function setObsoleteAll(bool) {
    forEachImage((img) => img.setObsolete(bool));
}


function refreshAll(changeDesc) { // changeDesc: "thresholdInc" | "thresholdDec" | "area"
    setObsoleteAll(true);
    if (!changeDesc) changeDesc = "area";
    if (changeDesc === "thresholdInc") {
        forEachImage((img) => {
            img.areaThresholdPixels.critical = img.areaThresholdPixels.critical.filter((pxl) => pxl.value > threshold);
            img.areaThresholdPixels.other = img.areaThresholdPixels.other.filter((pxl) => pxl.value > threshold);
            img.refresh();
        });
    } else if (changeDesc === "thresholdDec") {
        forEachImage((img) => {
            img.areaThresholdPixels.critical = img.areaPixels.critical.filter((pxl) => pxl.value > threshold);
            img.areaThresholdPixels.other = img.areaPixels.other.filter((pxl) => pxl.value > threshold);
            img.refresh();
        });
    } else if (changeDesc === "area") {
        forEachImage((img) => {
            img.areaPixels = areaSelect(img);
            img.areaThresholdPixels.critical = img.areaPixels.critical.filter((pxl) => pxl.value > threshold);
            img.areaThresholdPixels.other = img.areaPixels.other.filter((pxl) => pxl.value > threshold);
            img.refresh();
        });
    }
}
