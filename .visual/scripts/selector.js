let threshold = 10;

let xInset = 300;
let yInset = 300;
let isEllipse = true;

function areaSelect(image) {
    const { width, height } = image;
    const selectionWidth = width - 2 * xInset;
    const selectionHeight = height - 2 * yInset;
    const xInsetInverse = width - xInset;
    const yInsetInverse = height - yInset;

    const select = isEllipse ?
        (pixel) => {
            const { x, y } = pixel;
            // 1D-dist to center
            const xMidDist = Math.abs(width / 2 - x);
            const yMidDist = Math.abs(height / 2 - y);
            // Between 0 and 1 if in ellipse, making it a circle
            const xMidDistNormal = xMidDist / (selectionWidth / 2);
            const yMidDistNormal = yMidDist / (selectionHeight / 2);
            // Pythagorean formula to get 2D-dist to center
            // BUT: no need for 'sqrt', therefore better performance
            const dist = xMidDistNormal ** 2 + yMidDistNormal ** 2;
            // const dist = Math.sqrt(xMidDistNormal ** 2 + yMidDistNormal ** 2);
            // returns 'true' if pixel is in ellipse
            return dist <= 1;
        } :
        (pixel) => {
            const { x, y } = pixel;
            return (
                x >= xInset &&
                x <= xInsetInverse &&
                y >= yInset &&
                y <= yInsetInverse
            );
        };
    const pixels = image.deadPixels;
    const critical = pixels.filter((pixel) => select(pixel));
    const other = pixels.filter((pixel) => !select(pixel));
    return { critical, other };
}

const DEAD_PIXEL_NUM_MAX = 50; // >= would lead to full width
const MAX_WIDTH_PERCENT = 85;
/** Returns a map with critical and other width, values are between 0 and 100 */
function getBarPercentages(criticalNum, otherNum) {
    const sum = criticalNum + otherNum;

    let critical = criticalNum / DEAD_PIXEL_NUM_MAX * MAX_WIDTH_PERCENT;
    let other = otherNum / DEAD_PIXEL_NUM_MAX * MAX_WIDTH_PERCENT;

    if (sum > DEAD_PIXEL_NUM_MAX) {
        const factor = DEAD_PIXEL_NUM_MAX / sum;
        critical *= factor;
        other *= factor;
    }

    return { critical, other };
}
