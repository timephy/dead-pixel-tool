document.addEventListener("click", function (e) {
    const className = e.target.className;
    //console.log(e.target.className);
    if (className === "add-image-plus" ||
        className === "add-image-text" ||
        className === "image-item-add"
    ) {
        // add image
        addImageEvent();
    } else if (className === "remove-image") {
        // remove the image-group
        removeImageEvent(e.target);
    } else if (className === "remove-all-images-btn") {
        // remove all images
        removeAllImagesEvent();
    }
});

document.addEventListener("change", function (e) {
    const changeTargetId = e.target.id;
    console.log("Val changed", changeTargetId);

    if (changeTargetId === "ellipse") {
        isEllipse = e.target.checked;
        refreshAll("area");
    } else if (changeTargetId === "threshold") {
        const newThreshold = e.target.value;
        if (newThreshold > 255) {
            newThreshold = e.target.value = 250;
        } else if (newThreshold < 10) {
            newThreshold = e.target.value = 10;
        }

        if (newThreshold > threshold) {
            threshold = newThreshold;
            refreshAll("thresholdInc");
        } else {
            threshold = newThreshold;
            refreshAll("thresholdDec");
        }
    } else if (changeTargetId === "x-inset") {
        xInset = e.target.value < 0 ? 0 : e.target.value;
        refreshAll("area");
    } else if (changeTargetId === "y-inset") {
        yInset = e.target.value < 0 ? 0 : e.target.value;
        refreshAll("area");
    }
});

// Init
consumeImageJSON(deadPixelData);
